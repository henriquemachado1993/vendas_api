# Sobre
WEB Api genérica com SQL server

## Tecnologias utilizadas e versões

|                           |Versão            |
|---------------------------|------------------|
|Visual Studio Community    |	2017 		   |
|.Net Core    	            |	2.2 		   |
|SQL server Express  	    |	 			   |


## Gerar o banco de dados usando **Migrations**
**Observação:** As migrations necessárias já foram geradas, basta gerar o banco de dados.

Vá no menu de ferramentas do seu Visual Studio e procure por **Ferramentas-> Gerenciador de Pacotes Nuget -> Console do Gerenciador de Pacotes** e execute o comando abaixo:
    
    Update-Database
    
Agora, vamos verificar se o banco de dados foi criado. No menu de ferramentas abra **Exibir -> SQL Server Object Explorer** . Tabelas geradas **(Department, Sales e Seller)**

## Testando API
Para realizar testes, faça o download do programa **[Postman](https://www.getpostman.com/downloads/)**

Rotas para testar (Exemplo)

|           Rota            |    Metodo     | Descrição                                             |
|---------------------------|---------------|-------------------------------------------------------|       
|servidor/(Controlador)     |	 GET 		| Busca todos os registros                              |
|servidor/(Controlador)/1   |	 GET 		| Busca um registro específico                          |
|servidor/(Controlador)  	|	 POST		| Realiza um cadastro  (Usa json para enviar os dados)  |
|servidor/(Controlador)/1  	|	 PUT		| Raaliza uma Alteração (Usa json para enviar os dados)  |
|servidor/(Controlador)/1  	|	 DELETE		| Delete um registro                                    |
