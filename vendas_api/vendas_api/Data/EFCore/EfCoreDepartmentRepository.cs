﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vendas_api.Models;

namespace vendas_api.Data.EFCore
{
    public class EfCoreDepartmentRepository : Repository<Department, DBContext>
    {
        public EfCoreDepartmentRepository(DBContext context) : base(context)
        {

        }
    }
}
