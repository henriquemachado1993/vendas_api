﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vendas_api.Models;

namespace vendas_api.Data.EFCore
{
    public class EfCoreSalesRepository : Repository<Sales, DBContext>
    {
        public EfCoreSalesRepository(DBContext context) : base(context)
        {

        }
    }
}
