﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vendas_api.Models;

namespace vendas_api.Data.EFCore
{
    public class EfCoreSellerRepository : Repository<Seller, DBContext>
    {
        public EfCoreSellerRepository(DBContext context) : base(context)
        {

        }
    }
}
