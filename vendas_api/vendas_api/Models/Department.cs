﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using vendas_api.Data;

namespace vendas_api.Models
{
    public class Department : IEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} Required")]
        public string Name { get; set; }

        // Association
        public ICollection<Seller> Sellers { get; set; } = new List<Seller>();
    }
}
