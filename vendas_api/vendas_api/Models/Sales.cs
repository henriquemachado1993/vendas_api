﻿using System;
using System.ComponentModel.DataAnnotations;
using vendas_api.Data;

namespace vendas_api.Models
{
    public class Sales : IEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} Required")]      
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "{0} Required")]        
        public double Amount { get; set; }

        [Required(ErrorMessage = "{0} Required")]
        public string Status { get; set; }

        // Association
        public Seller Seller { get; set; }

        [Required(ErrorMessage = "{0} Required")]
        public int SellerId { get; set; }              
    }
}
