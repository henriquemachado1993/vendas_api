﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using vendas_api.Data;

namespace vendas_api.Models
{
    public class Seller : IEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} Required")]        
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} Required")]      
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} Required")]
       
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "{0} Required")]        
        public double BaseSalary { get; set; }

        // Association
        public Department Department { get; set; }
                
        public int DepartmentId { get; set; }
        public ICollection<Sales> Sales { get; set; } = new List<Sales>();

    }
}
