﻿using Microsoft.AspNetCore.Mvc;
using vendas_api.Controllers.BaseController;
using vendas_api.Data.EFCore;
using vendas_api.Models;

namespace vendas_api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class SellerController : BaseController<Seller, EfCoreSellerRepository>
    {
        public SellerController(EfCoreSellerRepository repository) : base(repository)
        {
        }
    }
}